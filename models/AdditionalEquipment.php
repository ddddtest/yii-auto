<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additional_equipment".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 *
 * @property AdditionalEquipmentCategory $category
 * @property AdsAddictionalEquipment[] $adsAddictionalEquipments
 */
class AdditionalEquipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'additional_equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['category_id'], 'integer'],
            [['name'], 'string', 'max' => 65],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalEquipmentCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(AdditionalEquipmentCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdsAddictionalEquipments()
    {
        return $this->hasMany(AdsAddictionalEquipment::className(), ['equipment_id' => 'id']);
    }
}
