<?php

namespace app\models;

use app\components\ImageHelper;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\db\Exception;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "car_photos".
 *
 * @property int $id
 * @property int $adv_id
 * @property string $filename
 * @property string $thumbnail
 *
 * @property SaleAds $adv
 */
class CarPhotos extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            /*[['adv_id', 'filename', 'thumbnail'], 'required'],
            [['adv_id'], 'integer'],
            [['thumbnail'], 'string'],
            [['filename'], 'string', 'max' => 300],
            [['adv_id'], 'exist', 'skipOnError' => true, 'targetClass' => SaleAds::className(), 'targetAttribute' => ['adv_id' => 'id']],*/
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 3]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adv_id' => 'Adv ID',
            'filename' => 'Filename',
            'thumbnail' => 'Thumbnail',
            'imageFiles' => 'Фото',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $v1 = Uuid::uuid4();
                $newName = $v1->toString();

                $photo = new CarPhotos([
                    'filename' => $newName . "." . $file->extension,
                    'thumbnail' => 'original',
                    'adv_id' => $this->adv_id,
                    'imageFiles' => $this->imageFiles,
                ]);
                $file->saveAs(Url::to('@app/uploads/') . $newName . '.' . $file->extension, false);
                $photo->save();

                for ($i = 0; $i < 2; $i++) {
                    $size = $i == 0 ? 'large' : 'small';

                    $thumbnail = new CarPhotos([
                        'filename' => $newName . '_' . $size . "." . $file->extension,
                        'thumbnail' => $size,
                        'adv_id' => $this->adv_id,
                        'imageFiles' => $this->imageFiles,
                    ]);

                    ImageHelper::thumbnail(
                        Url::to('@app/uploads/') . $photo->filename,
                        Url::to('@app/uploads/') . $newName . "_" . $size . "." . $file->extension,
                        $i
                    );
                    $thumbnail->save();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdv()
    {
        return $this->hasOne(SaleAds::className(), ['id' => 'adv_id']);
    }
}
