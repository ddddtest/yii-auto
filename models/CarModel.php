<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id
 * @property string $name
 * @property int $manufactor_id
 * @property string $engine_type
 * @property int $engine_cap
 * @property string $transmission_type
 * @property string $wheel_type
 *
 * @property CarManufacturer $manufactor
 * @property SaleAd[] $saleAds
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'manufactor_id', 'engine_type', 'engine_cap', 'transmission_type', 'wheel_type'], 'required'],
            [['manufactor_id', 'engine_cap'], 'integer'],
            [['name', 'engine_type', 'transmission_type', 'wheel_type'], 'string', 'max' => 65],
            [['manufactor_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarManufacturer::className(), 'targetAttribute' => ['manufactor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'manufactor_id' => 'Manufactor ID',
            'engine_type' => 'Engine Type',
            'engine_cap' => 'Engine Cap',
            'transmission_type' => 'Transmission Type',
            'wheel_type' => 'Wheel Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufactor()
    {
        return $this->hasOne(CarManufacturer::className(), ['id' => 'manufactor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleAds()
    {
        return $this->hasMany(SaleAd::className(), ['car_id' => 'id']);
    }

    public function getOriginalPhoto($adv_id) {
        $photos = \app\models\CarPhotos::find()->where(['adv_id' => $adv_id, 'thumbnail' => 'original'])->one();
        return $photos;
    }

    public function getThumbnails($adv_id) {
        $large = \app\models\CarPhotos::find()->where(['adv_id' => $adv_id, 'thumbnail' => 'large'])->one();
        $small = \app\models\CarPhotos::find()->where(['adv_id' => $adv_id, 'thumbnail' => 'small'])->one();
        return [$large, $small];
    }

    public function getAllThumbnails($adv_id) {
        $large = \app\models\CarPhotos::find()->where(['adv_id' => $adv_id, 'thumbnail' => 'large'])->all();
        $small = \app\models\CarPhotos::find()->where(['adv_id' => $adv_id, 'thumbnail' => 'small'])->all();
        return [$large, $small];
    }
}
