<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sale_ads".
 *
 * @property int $id
 * @property int $car_id
 * @property int $price
 * @property int $city_id
 * @property int $mileage
 * @property int $year
 * @property string $phone
 *
 * @property AdsAddictionalEquipment[] $adsAddictionalEquipments
 * @property CarPhotos[] $carPhotos
 * @property CarModel $car
 * @property Cities $city
 */
class SaleAds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_ads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'price', 'mileage', 'year', 'phone'], 'required'],
            [['car_id', 'price', 'mileage', 'year'], 'integer'],
            [['phone'], 'string', 'max' => 65],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModel::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => 'Car ID',
            'price' => 'Цена',
            'city_id' => 'Город',
            'mileage' => 'Пробег',
            'year' => 'Год',
            'phone' => 'Телефон',
            'addictional_equipment' => 'Дополнительное оборудование',
        ];
    }

    public function upload()
    {
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdsAddictionalEquipments()
    {
        return $this->hasMany(AdsAddictionalEquipment::className(), ['adv_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarPhotos()
    {
        return $this->hasMany(CarPhotos::className(), ['adv_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(CarModel::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddictionalEquipment()
    {
        return $this->hasOne(AdsAddictionalEquipment::className(), ['adv_id' => 'addictional_equipment']);
    }
}
