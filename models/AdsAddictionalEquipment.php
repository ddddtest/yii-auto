<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ads_addictional_equipment".
 *
 * @property int $id
 * @property int $equipment_id
 * @property int $adv_id
 *
 * @property AdditionalEquipment $equipment
 * @property SaleAds $adv
 * @property SaleAds[] $saleAds
 */
class AdsAddictionalEquipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ads_addictional_equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['equipment_id', 'adv_id'], 'required'],
            [['equipment_id', 'adv_id'], 'integer'],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalEquipment::className(), 'targetAttribute' => ['equipment_id' => 'id']],
            [['adv_id'], 'exist', 'skipOnError' => true, 'targetClass' => SaleAds::className(), 'targetAttribute' => ['adv_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'equipment_id' => 'Equipment ID',
            'adv_id' => 'Adv ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(AdditionalEquipment::className(), ['id' => 'equipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdv()
    {
        return $this->hasOne(SaleAds::className(), ['id' => 'adv_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleAds()
    {
        return $this->hasMany(SaleAds::className(), ['addictional_equipment' => 'adv_id']);
    }
}
