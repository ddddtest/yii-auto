create table additional_equipment
(
    id          int(11)
        constraint additional_equipment_pk
            primary key,
    category_id int(11)     not null
        references additional_equipment_categories,
    name        varchar(65) not null
);

INSERT INTO additional_equipment (id, category_id, name) VALUES (1, 1, 'ABS');
INSERT INTO additional_equipment (id, category_id, name) VALUES (2, 1, 'Крепление детского сидения ISOFIX');
INSERT INTO additional_equipment (id, category_id, name) VALUES (3, 1, 'Сигнализация');
INSERT INTO additional_equipment (id, category_id, name) VALUES (4, 2, 'Противотуманные фары');
INSERT INTO additional_equipment (id, category_id, name) VALUES (5, 2, 'Тонированные стекла');
INSERT INTO additional_equipment (id, category_id, name) VALUES (6, 4, 'Сабвуфер');
INSERT INTO additional_equipment (id, category_id, name) VALUES (7, 3, 'Автономный отопитель салона');
INSERT INTO additional_equipment (id, category_id, name) VALUES (8, 3, 'Бортовой компьютер');
INSERT INTO additional_equipment (id, category_id, name) VALUES (9, 3, 'Круиз контроль');
INSERT INTO additional_equipment (id, category_id, name) VALUES (10, 3, 'Омыватель фар');
create table additional_equipment_categories
(
    id   int(11)
        constraint additional_equipment_categories_pk
            primary key,
    name varchar(65) not null
);

INSERT INTO additional_equipment_categories (id, name) VALUES (1, 'Безопасность');
INSERT INTO additional_equipment_categories (id, name) VALUES (2, 'Экстерьер');
INSERT INTO additional_equipment_categories (id, name) VALUES (3, 'Комфорт');
INSERT INTO additional_equipment_categories (id, name) VALUES (4, 'Мультимедиа');
create table ads_addictional_equipment
(
    id           integer
        constraint ads_addictional_equipment_pk
            primary key autoincrement,
    equipment_id int(11) not null,
    adv_id       int(11) not null
);

INSERT INTO ads_addictional_equipment (id, equipment_id, adv_id) VALUES (83, 6, 104);
INSERT INTO ads_addictional_equipment (id, equipment_id, adv_id) VALUES (84, 6, 105);
create table car_manufacturer
(
    id   int(11)
        constraint car_manufacturer_pk
            primary key,
    name varchar(65) not null
);

INSERT INTO car_manufacturer (id, name) VALUES (1, 'Porsche');
INSERT INTO car_manufacturer (id, name) VALUES (2, 'Volkswagen');
INSERT INTO car_manufacturer (id, name) VALUES (3, 'Skoda');
INSERT INTO car_manufacturer (id, name) VALUES (4, 'Audi');
create table car_model
(
    id                int(11)
        constraint car_model_pk
            primary key,
    name              varchar(65) not null,
    manufactor_id     int(11)     not null
        references car_manufacturer,
    engine_type       varchar(65) not null,
    engine_cap        int(11)     not null,
    transmission_type varchar(65) not null,
    wheel_type        varchar(65) not null
);

INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (1, 'Q5', 4, '123', 2000, '123', '123123');
INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (2, 'Rapid', 3, '123', 1400, 'auto', '123123');
INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (3, 'Golf', 2, '123', 123, '123', '123');
INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (4, 'Polo', 2, '123', 123, '123', '123');
INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (5, 'Tiguan', 2, '123', 123, '123', '123');
INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (6, 'Cayenne Turbo', 1, '123', 123, '312312', '312312');
INSERT INTO car_model (id, name, manufactor_id, engine_type, engine_cap, transmission_type, wheel_type) VALUES (7, 'Macan S', 1, '1233', 132132, '1312312', '321231');
create table car_photos
(
    id        integer
        constraint car_photos_pk
            primary key autoincrement,
    adv_id    int(11)      not null
        references sale_ads,
    filename  varchar(300) not null,
    thumbnail TEXT
);

INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (51, 104, '1568805062_553642154.jpg', 'original');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (52, 104, '1568805062_553642154_large.jpg', 'large');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (53, 104, '1568805062_553642154_small.jpg', 'small');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (54, 105, '1568805147_21985963.jpg', 'original');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (55, 105, '1568805147_21985963_large.jpg', 'large');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (56, 105, '1568805147_21985963_small.jpg', 'small');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (88, 233, 'c38c9fa5-4527-4daa-9eff-4e3dc79604b4.jpg', 'original');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (89, 233, 'c38c9fa5-4527-4daa-9eff-4e3dc79604b4_large.jpg', 'large');
INSERT INTO car_photos (id, adv_id, filename, thumbnail) VALUES (90, 233, 'c38c9fa5-4527-4daa-9eff-4e3dc79604b4_small.jpg', 'small');
create table cities
(
    id   integer
        constraint cities_pk
            primary key autoincrement,
    name varchar(65) not null
);

INSERT INTO cities (id, name) VALUES (1, 'Пятигорск');
INSERT INTO cities (id, name) VALUES (2, 'Ростов-на-Дону');
INSERT INTO cities (id, name) VALUES (3, 'Ставрополь');
INSERT INTO cities (id, name) VALUES (4, 'Краснодар');
create table migration
(
    version    varchar(180) not null
        primary key,
    apply_time integer
);

INSERT INTO migration (version, apply_time) VALUES ('m000000_000000_base', 1569261688);
create table sale_ads
(
    id      integer
        constraint sale_ads_pk
            primary key autoincrement,
    car_id  int(11)     not null
        references car_model,
    price   int(11)     not null,
    city_id int(11)     not null
        references cities,
    mileage int(11)     not null,
    year    int(11)     not null,
    phone   varchar(65) not null
);

INSERT INTO sale_ads (id, car_id, price, city_id, mileage, year, phone) VALUES (233, 6, 1337, 1, 1337, 0, '1337');
create table sale_ads_dg_tmp
(
    id                    integer
        constraint sale_ads_pk
            primary key autoincrement,
    car_id                int(11)     not null
        references car_model,
    price                 int(11)     not null,
    city_id               int(11)     not null
        references cities,
    mileage               int(11)     not null,
    year                  int(11)     not null,
    phone                 varchar(65) not null,
    addictional_equipment int(11) default NULL
);


-- No source text available
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'migration', 'migration', 2, 'CREATE TABLE `migration` (
	`version` varchar(180) NOT NULL PRIMARY KEY,
	`apply_time` integer
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'sqlite_autoindex_migration_1', 'migration', 3, null);
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'car_model', 'car_model', 6, 'CREATE TABLE "car_model"
(
	id int(11)
		constraint car_model_pk
			primary key,
	name varchar(65) not null,
	manufactor_id int(11) not null
		references car_manufacturer,
	engine_type varchar(65) not null,
	engine_cap int(11) not null,
	transmission_type varchar(65) not null,
	wheel_type varchar(65) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'sqlite_autoindex_car_model_1', 'car_model', 21, null);
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'car_manufacturer', 'car_manufacturer', 5, 'CREATE TABLE "car_manufacturer"
(
	id int(11)
		constraint car_manufacturer_pk
			primary key,
	name varchar(65) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'sqlite_autoindex_car_manufacturer_1', 'car_manufacturer', 15, null);
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'additional_equipment_categories', 'additional_equipment_categories', 7, 'CREATE TABLE "additional_equipment_categories"
(
	id int(11)
		constraint additional_equipment_categories_pk
			primary key,
	name varchar(65) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'sqlite_autoindex_additional_equipment_categories_1', 'additional_equipment_categories', 20, null);
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'additional_equipment', 'additional_equipment', 4, 'CREATE TABLE "additional_equipment"
(
	id int(11)
		constraint additional_equipment_pk
			primary key,
	category_id int(11) not null
		references additional_equipment_categories,
	name varchar(65) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'sqlite_autoindex_additional_equipment_1', 'additional_equipment', 14, null);
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'sqlite_sequence', 'sqlite_sequence', 13, 'CREATE TABLE sqlite_sequence(name,seq)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'cities', 'cities', 10, 'CREATE TABLE "cities"
(
	id integer
		constraint cities_pk
			primary key autoincrement,
	name varchar(65) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'ads_addictional_equipment', 'ads_addictional_equipment', 9, 'CREATE TABLE "ads_addictional_equipment"
(
	id integer
		constraint ads_addictional_equipment_pk
			primary key autoincrement,
	equipment_id int(11) not null,
	adv_id int(11) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'sale_ads_dg_tmp', 'sale_ads_dg_tmp', 12, 'CREATE TABLE sale_ads_dg_tmp
(
	id integer
		constraint sale_ads_pk
			primary key autoincrement,
	car_id int(11) not null
		references car_model,
	price int(11) not null,
	city_id int(11) not null
		references cities,
	mileage int(11) not null,
	year int(11) not null,
	phone varchar(65) not null,
	addictional_equipment int(11) default NULL
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'sale_ads', 'sale_ads', 19, 'CREATE TABLE "sale_ads"
(
	id integer
		constraint sale_ads_pk
			primary key autoincrement,
	car_id int(11) not null
		references car_model,
	price int(11) not null,
	city_id int(11) not null
		references cities,
	mileage int(11) not null,
	year int(11) not null,
	phone varchar(65) not null
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'car_photos', 'car_photos', 8, 'CREATE TABLE "car_photos"
(
	id integer
		constraint car_photos_pk
			primary key autoincrement,
	adv_id int(11) not null
		references sale_ads,
	filename varchar(300) not null,
	thumbnail TEXT
)');
-- No source text available
INSERT INTO sqlite_sequence (name, seq) VALUES ('cities', 4);
INSERT INTO sqlite_sequence (name, seq) VALUES ('ads_addictional_equipment', 226);
INSERT INTO sqlite_sequence (name, seq) VALUES ('sale_ads', 233);
INSERT INTO sqlite_sequence (name, seq) VALUES ('car_photos', 90);