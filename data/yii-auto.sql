-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: db
-- Время создания: Сен 18 2019 г., 12:23
-- Версия сервера: 10.3.2-MariaDB-10.3.2+maria~jessie
-- Версия PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii-auto`
--

-- --------------------------------------------------------

--
-- Структура таблицы `additional_equipment`
--

CREATE TABLE `additional_equipment` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `additional_equipment`
--

INSERT INTO `additional_equipment` (`id`, `category_id`, `name`) VALUES
(1, 1, 'ABS'),
(2, 1, 'Крепление детского сидения ISOFIX'),
(3, 1, 'Сигнализация'),
(4, 2, 'Противотуманные фары'),
(5, 2, 'Тонированные стекла'),
(6, 4, 'Сабвуфер'),
(7, 3, 'Автономный отопитель салона'),
(8, 3, 'Бортовой компьютер'),
(9, 3, 'Круиз контроль'),
(10, 3, 'Омыватель фар');

-- --------------------------------------------------------

--
-- Структура таблицы `additional_equipment_categories`
--

CREATE TABLE `additional_equipment_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `additional_equipment_categories`
--

INSERT INTO `additional_equipment_categories` (`id`, `name`) VALUES
(1, 'Безопасность'),
(2, 'Экстерьер'),
(3, 'Комфорт'),
(4, 'Мультимедиа');

-- --------------------------------------------------------

--
-- Структура таблицы `ads_addictional_equipment`
--

CREATE TABLE `ads_addictional_equipment` (
  `id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `ads_addictional_equipment`
--

INSERT INTO `ads_addictional_equipment` (`id`, `equipment_id`, `adv_id`) VALUES
(83, 6, 104),
(84, 6, 105),
(120, 1, 137),
(121, 3, 137),
(122, 4, 137),
(123, 7, 137),
(124, 6, 137);

-- --------------------------------------------------------

--
-- Структура таблицы `car_manufacturer`
--

CREATE TABLE `car_manufacturer` (
  `id` int(11) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `car_manufacturer`
--

INSERT INTO `car_manufacturer` (`id`, `name`) VALUES
(1, 'Porsche'),
(2, 'Volkswagen'),
(3, 'Skoda'),
(4, 'Audi');

-- --------------------------------------------------------

--
-- Структура таблицы `car_model`
--

CREATE TABLE `car_model` (
  `id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `manufactor_id` int(11) NOT NULL,
  `engine_type` varchar(65) CHARACTER SET utf8 NOT NULL,
  `engine_cap` int(11) NOT NULL,
  `transmission_type` varchar(65) CHARACTER SET utf8 NOT NULL,
  `wheel_type` varchar(65) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `car_model`
--

INSERT INTO `car_model` (`id`, `name`, `manufactor_id`, `engine_type`, `engine_cap`, `transmission_type`, `wheel_type`) VALUES
(1, 'Q5', 4, '123', 2000, '123', '123123'),
(2, 'Rapid', 3, '123', 1400, 'auto', '123123'),
(3, 'Golf', 2, '123', 123, '123', '123'),
(4, 'Polo', 2, '123', 123, '123', '123'),
(5, 'Tiguan', 2, '123', 123, '123', '123'),
(6, 'Cayenne Turbo', 1, '123', 123, '312312', '312312'),
(7, 'Macan S', 1, '1233', 132132, '1312312', '321231');

-- --------------------------------------------------------

--
-- Структура таблицы `car_photos`
--

CREATE TABLE `car_photos` (
  `id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL,
  `filename` varchar(300) CHARACTER SET utf8 NOT NULL,
  `thumbnail` enum('original','small','large','') CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `car_photos`
--

INSERT INTO `car_photos` (`id`, `adv_id`, `filename`, `thumbnail`) VALUES
(51, 104, '1568805062_553642154.jpg', 'original'),
(52, 104, '1568805062_553642154_large.jpg', 'large'),
(53, 104, '1568805062_553642154_small.jpg', 'small'),
(54, 105, '1568805147_21985963.jpg', 'original'),
(55, 105, '1568805147_21985963_large.jpg', 'large'),
(56, 105, '1568805147_21985963_small.jpg', 'small'),
(135, 137, '1568808868_56769504.jpg', 'original'),
(136, 137, '1568808868_56769504_large.jpg', 'large'),
(137, 137, '1568808868_56769504_small.jpg', 'small'),
(138, 137, '1568808870_1418954544.jpg', 'original'),
(139, 137, '1568808870_1418954544_large.jpg', 'large'),
(140, 137, '1568808870_1418954544_small.jpg', 'small'),
(141, 137, '1568808872_585651548.jpg', 'original'),
(142, 137, '1568808872_585651548_large.jpg', 'large'),
(143, 137, '1568808872_585651548_small.jpg', 'small');

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Пятигорск'),
(2, 'Ростов-на-Дону'),
(3, 'Ставрополь'),
(4, 'Краснодар');

-- --------------------------------------------------------

--
-- Структура таблицы `sale_ads`
--

CREATE TABLE `sale_ads` (
  `id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `mileage` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `phone` varchar(65) CHARACTER SET utf8 NOT NULL,
  `addictional_equipment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `sale_ads`
--

INSERT INTO `sale_ads` (`id`, `car_id`, `price`, `city_id`, `mileage`, `year`, `phone`, `addictional_equipment`) VALUES
(137, 6, 30000, 1, 10000, 2019, '88005553535', 137);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `additional_equipment`
--
ALTER TABLE `additional_equipment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `additional_equipment_categories`
--
ALTER TABLE `additional_equipment_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ads_addictional_equipment`
--
ALTER TABLE `ads_addictional_equipment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ads_addictional_equipment_ibfk_1` (`equipment_id`),
  ADD KEY `ads_addictional_equipment_ibfk_2` (`adv_id`);

--
-- Индексы таблицы `car_manufacturer`
--
ALTER TABLE `car_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `manufactor_id` (`manufactor_id`);

--
-- Индексы таблицы `car_photos`
--
ALTER TABLE `car_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `car_photos_ibfk_1` (`adv_id`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sale_ads`
--
ALTER TABLE `sale_ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `car_id` (`car_id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `sale_ads_ibfk_3` (`addictional_equipment`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `additional_equipment`
--
ALTER TABLE `additional_equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `additional_equipment_categories`
--
ALTER TABLE `additional_equipment_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `ads_addictional_equipment`
--
ALTER TABLE `ads_addictional_equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT для таблицы `car_manufacturer`
--
ALTER TABLE `car_manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `car_model`
--
ALTER TABLE `car_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `car_photos`
--
ALTER TABLE `car_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `sale_ads`
--
ALTER TABLE `sale_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `additional_equipment`
--
ALTER TABLE `additional_equipment`
  ADD CONSTRAINT `additional_equipment_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `additional_equipment_categories` (`id`);

--
-- Ограничения внешнего ключа таблицы `ads_addictional_equipment`
--
ALTER TABLE `ads_addictional_equipment`
  ADD CONSTRAINT `ads_addictional_equipment_ibfk_1` FOREIGN KEY (`equipment_id`) REFERENCES `additional_equipment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ads_addictional_equipment_ibfk_2` FOREIGN KEY (`adv_id`) REFERENCES `sale_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `car_model`
--
ALTER TABLE `car_model`
  ADD CONSTRAINT `car_model_ibfk_1` FOREIGN KEY (`manufactor_id`) REFERENCES `car_manufacturer` (`id`);

--
-- Ограничения внешнего ключа таблицы `car_photos`
--
ALTER TABLE `car_photos`
  ADD CONSTRAINT `car_photos_ibfk_1` FOREIGN KEY (`adv_id`) REFERENCES `sale_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `sale_ads`
--
ALTER TABLE `sale_ads`
  ADD CONSTRAINT `sale_ads_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `car_model` (`id`),
  ADD CONSTRAINT `sale_ads_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `sale_ads_ibfk_3` FOREIGN KEY (`addictional_equipment`) REFERENCES `ads_addictional_equipment` (`adv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
