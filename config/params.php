<?php
Yii::setAlias('@app', realpath(dirname(__FILE__).'/../../'));
Yii::setAlias('@uploadPath', realpath(dirname(__FILE__).'/../../uploads'));

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'app' => realpath(dirname(__FILE__).'/../../'),
];
