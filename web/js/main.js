var id = 1;

const deleteAdv = (elemId) => {
    let id = elemId.split("-")[1];

    $.ajax({
        url: '?r=ads/delete',
        data: {id: id},
        success: function(e) {
            if (e.result === true) {
                $("#adv-" + id).remove();
            } else {
                console.log(e.error);
            }
        }
    });

    return false;
};

$(document).ready(function() {
    const fillList = (id) => {
        $.ajax({
            url: '?r=car/models',
            data: {manufactorId: id},
            success: function(e) {
                $('#carModel').html('');

                [].forEach.call(e, function(a) {
                    $("#carModel").append(`<option value="${a.id}">${a.name}</option>`);
                });
            }
        });
    };

    if ($("#carManufactor")) {
        $("#carManufactor").change(function(e) {
            id = $(this).children(":selected").attr("id");
            $("#manufactorId").attr("value", id);

            fillList(id);
        });
        fillList(id);
    }
});