<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
])
?>
<form action="<?= Url::toRoute(['ads/add']) ?>" method="POST" enctype="multipart/form-data">
    <?= $form->errorSummary($model); ?>
    <div class="form-group">
        <input type="hidden" name="_csrf" value="<?= $csrfToken?>"/>
        <input type="hidden" id="manufactorId" name="manufactorId" value="1"/>
        <label for="carManufactor">Марка автомобиля</label>
        <select class="form-control" id="carManufactor" name="manufactor">
            <?php foreach ($manufactorers as $m) :?>
            <option name="manufactor" id="<?= $m->id ?>" value="<?= $m->id ?>"><?= $m->name ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="carModel">Модель автомобиля</label>
        <select class="form-control" id="carModel" name="SaleAds[car_id]">
        </select>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'mileage')->label('Пробег') ?>
    </div>
    <div class="form-group">
        <label for="mileage">Дополнительное оборудование</label>
        <br/>
        <?php foreach ($equipmentCategories as $cat): ?>
        <b><?= $cat->name ?></b>
        <select multiple class="form-control" name="SaleAds[equipment][]">
            <?php foreach ($cat->getAdditionalEquipments()->all() as $eq): ?>
            <option id="<?= $eq->id ?>" value="<?= $eq->id ?>"><?= $eq->name ?></option>
            <?php endforeach; ?>
        </select>
        <?php endforeach; ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'price')->label('Цена') ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'phone')->label('Телефон') ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'year')->label('Год')->dropDownList(range(1984, 2019)) ?>
    </div>
    <div class="form-group">
        <?php $cityList = array_column($cities, 'name') ?>
        <?php $cityList = array_combine(range(1, count($cityList)), $cityList); ?>
        <?= $form->field($model, 'city_id')->label('Город')->dropDownList($cityList) ?>
    </div>
    <div class="form-group">
        <?= $form->field($photos, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
    </div>
    <!--
    <div class="form-group">
        <label for="mileage">Фото</label>
        <input class="form-control" type="file" name="image[]">
        <input class="form-control" type="file" name="image[]">
        <input class="form-control" type="file" name="image[]">
    </div>
    -->
    <input type="submit" class="btn btn-success"/>
</form>