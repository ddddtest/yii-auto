<?php

namespace app\controllers;
use Yii;
use app\models;

class CarController extends \yii\web\Controller
{
    public function actionManufacturers()
    {
        $query = \app\models\CarManufacturer::find()->all();
        return $this->asJson($query);
    }

    public function actionModels()
    {
        $request = Yii::$app->request;

        $manufactorId = $request->get('manufactorId');
        $query = \app\models\CarModel::find()->where(['manufactor_id' => $manufactorId])->all();
        return $this->asJson($query);
    }
}
