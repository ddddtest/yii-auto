<?php

namespace app\controllers;

use app\components\FileUpload;
use app\components\ImageHelper;
use app\models\AdditionalEquipmentCategories;
use app\models\AdsAddictionalEquipment;
use app\models\CarManufacturer;
use app\models\CarPhotos;
use app\models\Cities;
use app\models\SaleAds;
use Ramsey\Uuid\Uuid;
use yii;
use yii\base\ErrorException;
use yii\data\Pagination;

use app\models;
use yii\helpers\Url;

class AdsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = SaleAds::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 5]);
        $ads = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('index.twig', ['ads' => $ads, 'pages' => $pages]);
    }

    public function actionView($id)
    {
        try {
            $adv = SaleAds::findOne($id);

            return $this->render('view.twig', ['adv' => $adv]);
        } catch (ErrorException $e) {
            return $this->asJson(["error" => $e->getMessage()]);
        }
    }

    public function actionDelete($id)
    {
        try {
            $adv = SaleAds::findOne($id);
            $adv_id = $adv->id;
            $adv->delete();
            $photos = CarPhotos::find()->where(['adv_id' => $adv_id])->all();
            foreach ($photos as $photo) {
                @unlink(\Yii::$app->basePath . '/uploads/' . $photo->filename);
            }

            $photos = CarPhotos::deleteAll(['adv_id' => $adv_id]);
            $addictional_equipment = AdsAddictionalEquipment::deleteAll(['adv_id' => $adv_id]);
            return $this->asJson(['result' => true]);
        } catch (ErrorException $e) {
            return $this->asJson(["error" => $e->getMessage()]);
        }
    }

    public function actionAdd()
    {
        $errors = [];
        $saleAd = new SaleAds();
        $photos = new CarPhotos();

        $manufactorer = CarManufacturer::find()->all();
        $equipmentCategories = AdditionalEquipmentCategories::find()->all();
        $cities = Cities::find()->all();

        // стандартные значения
        $saleAd->city_id = 1;
        $saleAd->year = 2019;

        $request = Yii::$app->request;

        if ($request->isPost) {
            $saleAd->load($request->post());

            $file = yii\web\UploadedFile::getInstances($photos, 'imageFiles');
            $photos->imageFiles = $file;
            $photos->validate();

            if (!$saleAd->validate(null, false) || !$photos->validate()) {
                $errors[] = $saleAd->errors;
            } else {
                $saleAd->save();
                $photos->adv_id = $saleAd->id;
                if ($photos->upload()) {
                    // Обработка дополнительного оборудования
                    $postEquipmentArray = isset($_POST['SaleAds']['equipment']) ? $_POST['SaleAds']['equipment'] : false;

                    if ($postEquipmentArray) {
                        foreach ($postEquipmentArray as $equipmentId) {
                            $newEquip = new AdsAddictionalEquipment([
                                'equipment_id' => intval($equipmentId),
                                'adv_id' => $saleAd->id
                            ]);
                            $newEquip->save();
                        }
                    }
                    return $this->redirect(Url::toRoute(['ads/view', 'id' => $saleAd->id]));
                }
            }
        }

        return $this->render('add.php', [
            'manufactorers' => $manufactorer,
            'equipmentCategories' => $equipmentCategories,
            'cities' => $cities,
            'csrfToken' => $request->csrfToken,
            'model' => $saleAd,
            'photos' => $photos,
        ]);
    }
}
