<?php

namespace app\components;

class ImageHelper
{
    public static function thumbnail($fileName, $savePath, $type = 0) {
        list($width_orig, $height_orig) = getimagesize($fileName);

        $im = imagecreatefromjpeg($fileName);
        $width = ImageSX($im);
        $height = ImageSY($im);
        $ratio = 4 / 3;
        $width_out = $width_orig;
        $height_out = $height_orig;

        if ($height_out * $ratio < $width_out) {
            $height_out = floor($width_out / $ratio);
        } else {
            $width_out = floor($height_out * $ratio);
        }

        $left = round(($width_out - $width) / 2);
        $top = round(($height_out - $height) / 2);
        $image_out = imagecreatetruecolor($width_out, $height_out);
        $bg_color = ImageColorAllocate($image_out, 255, 255, 255);
        imagefill($image_out, 0, 0, $bg_color);
        imagecopy($image_out, $im, $left, $top, 0, 0, $width, $height);

        if ($type == 0) {
            $newWidth = 720;
            $newHeight = 540;
        }

        if ($type == 1) {
            $newWidth = 146;
            $newHeight = 106;
        }

        $thumb = imagecreatetruecolor($newWidth, $newHeight);

        imagecopyresized($thumb, $image_out, 0, 0, 0, 0, $newWidth, $newHeight, $width_out, $height_out);

        // save image and clear memory
        imagejpeg($thumb, $savePath);
        imagedestroy($thumb);
        imagedestroy($image_out);
    }
}
